/**
 *  @author    Cesar Fuguet Tortolero
 *  @date      December 2019
 *  @brief     Compute PI using a montecarlo method
 */
#include <stdio.h>
#include <time.h>
#include <omp.h>

unsigned long lfsr_rand();

#define NTHREADS            NCORES
#define SAMPLES             50000ul
#define SAMPLES_PER_THREAD  (SAMPLES / NTHREADS)

/* Type definition a coordinate in the cartesian plan */
typedef struct coordinate_s {
	double x;
	double y;
} coordinate_t;

/* Type definition of a cacheblock-aligned counter */
typedef struct thread_count_s
{
	unsigned long val __attribute__(( aligned(64) ));
} thread_count_t;

/* Return a random point (x,y) with 0 < x < 1, 0 < y < 1 */
static inline coordinate_t random_point()
{
	coordinate_t ret;
	unsigned long value = lfsr_rand();
	
	ret.x = (double)(((value >>  0) & 0xfffful) - 1) / 0xfffful;
	ret.y = (double)(((value >> 16) & 0xfffful) - 1) / 0xfffful;
	return ret;
}

/* Vector of threads' private counters */
thread_count_t count_per_thread[NTHREADS];

int main()
{
	struct timespec t0, t1;
	unsigned long count;
	double pi;
	int i;

	omp_set_dynamic(0);
	omp_set_num_threads(NTHREADS);

	clock_gettime(CLOCK_MONOTONIC, &t0);

	#pragma omp parallel
	{
		int p;
		int tid = omp_get_thread_num();
		printf("thread %d computing...\n", tid);

		count_per_thread[tid].val = 0;
		for (p = 0; p < SAMPLES_PER_THREAD; p++) {
			coordinate_t point = random_point();
#if 0
			if (tid == 0)
				printf("(%f,%f)\n", point.x, point.y);
#endif
			if ((point.x*point.x + point.y*point.y) <= 1.0)
				count_per_thread[tid].val++;
		}
	}

	/* cumulate the computated value of each thread */
	count = 0;
	for (i = 0; i < NTHREADS; i++)
		count += count_per_thread[i].val;

	/* compute pi */
	pi = (double)(count*4) / (SAMPLES_PER_THREAD*NTHREADS);

	clock_gettime(CLOCK_MONOTONIC, &t1);

	printf("Pi value is approximately %f\n", pi);

	/*  statistics display */
	long unsigned int t0_ms, t1_ms, t_ms;
	t0_ms = t0.tv_sec*1000 + t0.tv_nsec/1000000;
	t1_ms = t1.tv_sec*1000 + t1.tv_nsec/1000000;
	t_ms = (t1_ms - t0_ms);
	printf("Time measurements:\n"
		">  computing (total)           %10lu milli-seconds\n", t_ms);

	return 0;
}
