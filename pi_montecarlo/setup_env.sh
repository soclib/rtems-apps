#!/bin/bash

binpath=/Users/cfuguet/Workspace/RTEMS_riscv_soclib/rtems-binaries
rtemsversion=5
rtemsplatform=riscv-rtems5/rv32-soclib-xcache

PATH=${binpath}/${rtemsversion}/bin:${PATH}
export PATH

MANPATH=${binpath}/${rtemsversion}/share/man:${MANPATH}
export MANPATH

RTEMS_MAKEFILE_PATH=${binpath}/${rtemsversion}/${rtemsplatform}
export RTEMS_MAKEFILE_PATH
