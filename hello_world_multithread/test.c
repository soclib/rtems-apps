#include <stdio.h>
#include <pthread.h>
#include <assert.h>
#include <sched.h>
#include <stdatomic.h>
#include <sys/time.h>
#include <rtems/score/cpu.h>

#define NWORKERS 8

pthread_t workers[NWORKERS];
atomic_ulong workers_done;

unsigned long long int clock_get_ms()
{
	struct timeval tv;
	unsigned long long int ms;

	gettimeofday(&tv, NULL);
	ms = (unsigned long long int)tv.tv_sec * 1000;
	ms += (unsigned long long int)tv.tv_usec / 1000;
	return ms;
}


void *worker_func(void *arg)
{

	(void)arg;

	printf("Hello World from worker thread (CPU %d)\n",
			_CPU_SMP_Get_current_processor());

	atomic_fetch_add_explicit(&workers_done, 1, memory_order_relaxed);

	/* worker threads never quit */
	for (;;) sched_yield();

	assert(false);
	pthread_exit(NULL);
}

int main()
{
	unsigned int i, c;
	unsigned long long int ms;

	printf("Test start\n");

	ms = clock_get_ms();

	printf("Hello World from master thread (CPU %d)\n",
			_CPU_SMP_Get_current_processor());

	atomic_init(&workers_done, c);

	for (i = 0; i < NWORKERS; i++) {
		int ret = pthread_create(&workers[i], NULL, worker_func, NULL);
		assert (ret == 0);
	}

	for (;;) {
		c = atomic_load_explicit(&workers_done, memory_order_relaxed);
		if (c == NWORKERS)
			break;

		sched_yield();
	}

	printf("Test end (took %llu milliseconds)\n", clock_get_ms() - ms);
	pthread_exit(NULL);
	return 0;
}

#ifdef __rtems__
void *POSIX_Init(void *arg)
{
	main();
	return NULL;
}
#endif

/* configuration information */
#define CONFIGURE_APPLICATION_NEEDS_SIMPLE_CONSOLE_DRIVER
#define CONFIGURE_APPLICATION_NEEDS_CLOCK_DRIVER

#define CONFIGURE_MICROSECONDS_PER_TICK 10000

#define CONFIGURE_MAXIMUM_PROCESSORS 4
#define CONFIGURE_MAXIMUM_POSIX_THREADS 16
#define CONFIGURE_POSIX_INIT_THREAD_TABLE

#define CONFIGURE_INIT
#include <rtems/confdefs.h>
/* end of file */
