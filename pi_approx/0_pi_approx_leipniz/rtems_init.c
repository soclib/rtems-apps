#ifdef __rtems__

#include <stdlib.h>

#ifndef NCORES
#error "NCORES shall be passed during compilation"
#endif

int main();

/* Entry point for RTEMS applications */
void* POSIX_Init(void *arg)
{
	main();
	return NULL;
}

/* RTEMS configuration information */
#define CONFIGURE_APPLICATION_NEEDS_SIMPLE_CONSOLE_DRIVER
#define CONFIGURE_APPLICATION_NEEDS_CLOCK_DRIVER
#define CONFIGURE_MICROSECONDS_PER_TICK 10000
#define CONFIGURE_UNLIMITED_OBJECTS
#define CONFIGURE_UNIFIED_WORK_AREAS
#define CONFIGURE_MAXIMUM_PROCESSORS NCORES
#define CONFIGURE_MAXIMUM_POSIX_THREADS 32
#define CONFIGURE_POSIX_INIT_THREAD_TABLE
#define CONFIGURE_INIT
#include <rtems/confdefs.h>
#endif
