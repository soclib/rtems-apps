/**
 *  @brief     PI compute application
 */
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <omp.h>

#define NCORES 1
#define NITERS 1000

double compute_pi()
{
    double partial_sums[NCORES];
    const int niters = NITERS/NCORES;

    #pragma omp parallel
    {
        const int tid = omp_get_thread_num();
        const int start = tid*niters;
        const int end = (tid == (NCORES-1)) ? NITERS : (tid+1)*niters;

        double sum = 0;
        int sign = 1;
        for (int i = start; i < end; i++) {
            sum = sum + sign*(4.0/(2*i + 1));
            sign = -sign;
        }

        partial_sums[tid] = sum;
    }

    // add all partial results (reduction operation)
    double result = 0;
    for (int i = 0; i < NCORES; i++) {
        result += partial_sums[i];
    }
    return result;
}

int main()
{
    struct timespec t0, t1;
    long unsigned int t0_ms, t1_ms;
    double pi;

    printf("Computing Pi with NCORES=%d\n", NCORES);

    /*  compute pi */
    printf("computing start\n");
    clock_gettime(CLOCK_MONOTONIC, &t0);
    pi = compute_pi();
    clock_gettime(CLOCK_MONOTONIC, &t1);
    printf("computing finish\n");

    /*  print the computed value of pi */
    printf("PI is approximately equal to %f\n", pi);

    /*  statistics display */
    t0_ms = t0.tv_sec*1000 + t0.tv_nsec/1000000;
    t1_ms = t1.tv_sec*1000 + t1.tv_nsec/1000000;
    printf("Time measurements:\n"
            ">  computing (total)     %10lu milli-seconds\n",
            (t1_ms - t0_ms));

    return 0;
}
