---
title: Example applications for the RTEMS kernel
author: César Fuguet-Tortolero
date: October, 2020
---

This repository contains simple examples that can be compiled for any given BSP
supported by the RTEMS kernel.

To be able to compile these applications you must:
    1. Install the crosscompiler for your target architecture with RTEMS
       libraries.
    2. Copy one of the scripts in the templates/ directory and put it in the
       root of this repository.
    3. Modify the copied script for specifying the exact location of your
       crosscompiler installation.
    4. "Source" the script
    5. Use the make command in the directory of the application you want to compile.
